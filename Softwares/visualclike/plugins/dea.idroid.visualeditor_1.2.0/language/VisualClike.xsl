<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:strip-space elements="*"/>

<xsl:template match="/">
<html>
<body>
<pre>
<xsl:apply-templates/>
</pre>
</body>
</html>
</xsl:template>

<xsl:template match="Program">
	<xsl:text>/**&#10; * </xsl:text>
	<xsl:value-of select="@Description"/>
	<xsl:text>&#10; * @author </xsl:text>
	<xsl:value-of select="@Author"/>
	<xsl:text>&#10; * @version </xsl:text>
	<xsl:value-of select="@Version"/>
	<xsl:text>&#10; */</xsl:text>
	<xsl:text>&#10;</xsl:text>
	<xsl:text>&#10;</xsl:text>
	
	<xsl:text>#include "c-like.h"</xsl:text>
	<xsl:text>&#10;</xsl:text>
	<xsl:text>#include "robot.h"</xsl:text>
	<xsl:text>&#10;</xsl:text>
	
	<!-- declare all counter variables -->
	<xsl:text>&#10;</xsl:text>
	<xsl:for-each select="Variable[@Type='Counter']">
		<xsl:text>&#10;counter </xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text> = new(counter);</xsl:text>
	</xsl:for-each>
	<!-- declare all timer variables -->
	<xsl:text>&#10;</xsl:text>
	<xsl:for-each select="Variable[@Type='Timer']">
		<xsl:text>&#10;timer </xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text>;</xsl:text>
	</xsl:for-each>
	<!-- declare all behaviors -->
	<xsl:for-each select="Behavior">
		<xsl:text>&#10;declare( behavior(</xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text>) )</xsl:text>
		<xsl:text>;</xsl:text>
	</xsl:for-each>
	<!-- declare procs -->
	<xsl:text>&#10;</xsl:text>
	<xsl:for-each select="Procedure">
		<xsl:text>&#10;void </xsl:text>
		<xsl:value-of select="@Name"/>
		<xsl:text>()</xsl:text>
		<xsl:text>;</xsl:text>
	</xsl:for-each>
	<!-- recursion -->
	<xsl:apply-templates/>
	<xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="Fragment">
	<!-- recursion -->
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="Main">
	<xsl:text>&#10;&#10;define( behavior(Main))</xsl:text>
	<xsl:value-of select="@Name"/>
	<xsl:call-template name="Scope"/>
</xsl:template>

<xsl:template match="Procedure">
	<xsl:text>&#10;&#10;void </xsl:text>
	<xsl:value-of select="@Name"/>
	<xsl:text>()</xsl:text>
	<xsl:call-template name="Scope"/>
</xsl:template>

<xsl:template match="Behavior">
	<xsl:text>&#10;&#10;define( behavior(</xsl:text>
	<xsl:value-of select="@Name"/>
	<xsl:text>) )</xsl:text>
	<xsl:call-template name="Scope"/>
</xsl:template>

<xsl:template name="OpenBrace">
	<xsl:text>&#10;</xsl:text>
	<xsl:for-each select="ancestor::If|ancestor-or-self::Switch|ancestor::While">
		<xsl:text>&#9;</xsl:text>
	</xsl:for-each>
	<xsl:text>{</xsl:text>
</xsl:template>

<xsl:template name="CloseBrace">
	<xsl:text>&#10;</xsl:text>
	<xsl:for-each select="ancestor::If|ancestor-or-self::Switch|ancestor::While">
		<xsl:text>&#9;</xsl:text>
	</xsl:for-each>
	<xsl:text>}</xsl:text>
</xsl:template>

<xsl:template name="IndentLess">
	<xsl:text>&#10;</xsl:text>
	<xsl:for-each select="ancestor::Fragment|ancestor::Main|ancestor::Procedure|ancestor::Behavior|ancestor::Loop|ancestor::Then|ancestor::Else|ancestor::Case|ancestor::Default">
		<xsl:text>&#9;</xsl:text>
	</xsl:for-each>
</xsl:template>

<xsl:template name="Indent">
	<xsl:text>&#10;</xsl:text>
	<xsl:for-each select="ancestor::Fragment|ancestor::Main|ancestor::Procedure|ancestor::Behavior|ancestor::Loop|ancestor::Then|ancestor::Else|ancestor-or-self::Case|ancestor-or-self::Default">
		<xsl:text>&#9;</xsl:text>
	</xsl:for-each>
</xsl:template>

<xsl:template name="Scope">
	<xsl:call-template name="OpenBrace"/>
	<xsl:apply-templates/>
	<xsl:call-template name="CloseBrace"/>
</xsl:template>

<!--Sensors-->

<xsl:template match="Temperature">
	<xsl:text>local(temperature)</xsl:text>
</xsl:template>

<xsl:template match="Battery">
	<xsl:text>local(battery).</xsl:text>
	<xsl:choose>
	<xsl:when test="@Source='Logic'"><xsl:text>logic</xsl:text></xsl:when>
	<xsl:when test="@Source='Motors'"><xsl:text>motors</xsl:text></xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="Proximity">
	<xsl:text>local(proximity).</xsl:text>
	<xsl:choose>
	<xsl:when test="@Source='Left'"><xsl:text>left</xsl:text></xsl:when>
	<xsl:when test="@Source='Center'"><xsl:text>center</xsl:text></xsl:when>
	<xsl:when test="@Source='Right'"><xsl:text>right</xsl:text></xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="Tracking">
	<xsl:text>local(vision).</xsl:text>
	<xsl:choose>
	<xsl:when test="@Coord='X'"><xsl:text>x</xsl:text></xsl:when>
	<xsl:when test="@Coord='Y'"><xsl:text>y</xsl:text></xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="AnalogIn">
	<xsl:text>local(analog_in).</xsl:text>
	<xsl:choose>
	<xsl:when test="@Source='1'"><xsl:text>pin_1</xsl:text></xsl:when>
	<xsl:when test="@Source='2'"><xsl:text>pin_2</xsl:text></xsl:when>
	</xsl:choose>
</xsl:template>

<xsl:template match="SoundDir">
	<xsl:text>local(sound_dir)</xsl:text>
</xsl:template>

<xsl:template match="VoiceCmd">
	<xsl:text>local(voice_cmd)</xsl:text>
</xsl:template>

<xsl:template match="FindMinDistance">
	<xsl:text>find_min_distance(local(proximity))</xsl:text>
</xsl:template>

<xsl:template match="FindMaxDistance">
	<xsl:text>find_max_distance(local(proximity))</xsl:text>
</xsl:template>

<xsl:template match="GetTrackingArea">
	<xsl:text>get_tracking_area(local(vision))</xsl:text>
</xsl:template>

<xsl:template match="Counter">
	<xsl:text>get(</xsl:text>
	<xsl:value-of select="@Name"/>
	<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Number">
	<xsl:value-of select="@Value"/>
</xsl:template>

<!--Conditions-->

<xsl:template match="InRange">
	<xsl:text>(</xsl:text>
	<xsl:apply-templates select="child::*[position()=2]"/>
	<xsl:text> &lt;= </xsl:text>
	<xsl:apply-templates select="child::*[position()=1]"/>
	<xsl:text> &amp;&amp; </xsl:text>
	<xsl:apply-templates select="child::*[position()=1]"/>
	<xsl:text> &lt;= </xsl:text>
	<xsl:apply-templates select="child::*[position()=3]"/>
	<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="OutOfRange">
	<xsl:text>(</xsl:text>
	<xsl:apply-templates select="child::*[position()=1]"/>
	<xsl:text> &lt; </xsl:text>
	<xsl:apply-templates select="child::*[position()=2]"/>
	<xsl:text> || </xsl:text>
	<xsl:apply-templates select="child::*[position()=3]"/>
	<xsl:text> &lt; </xsl:text>
	<xsl:apply-templates select="child::*[position()=1]"/>
	<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="LessThan">
	<xsl:text>(</xsl:text>
	<xsl:apply-templates select="child::*[position()=1]"/>
	<xsl:text> &lt; </xsl:text>
	<xsl:apply-templates select="child::*[position()=2]"/>
	<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="LessThanOrEqual">
	<xsl:text>(</xsl:text>
	<xsl:apply-templates select="child::*[position()=1]"/>
	<xsl:text> &lt;= </xsl:text>
	<xsl:apply-templates select="child::*[position()=2]"/>
	<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="EqualTo">
	<xsl:text>(</xsl:text>
	<xsl:apply-templates select="child::*[position()=1]"/>
	<xsl:text> == </xsl:text>
	<xsl:apply-templates select="child::*[position()=2]"/>
	<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="NotEqual">
	<xsl:text>(</xsl:text>
	<xsl:apply-templates select="child::*[position()=1]"/>
	<xsl:text> != </xsl:text>
	<xsl:apply-templates select="child::*[position()=2]"/>
	<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="GreaterThan">
	<xsl:text>(</xsl:text>
	<xsl:apply-templates select="child::*[position()=1]"/>
	<xsl:text> &gt; </xsl:text>
	<xsl:apply-templates select="child::*[position()=2]"/>
	<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="GreaterThanOrEqual">
	<xsl:text>(</xsl:text>
	<xsl:apply-templates select="child::*[position()=1]"/>
	<xsl:text> &gt;= </xsl:text>
	<xsl:apply-templates select="child::*[position()=2]"/>
	<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="UrcCompare">
	<xsl:text>(urc_compare(&amp;local(urc), </xsl:text>
	<xsl:value-of select="@Index"/>
	<xsl:text>))</xsl:text>
</xsl:template>

<!--Language-->

<xsl:template match="WaitFor">
	<xsl:call-template name="Indent"/>
	<xsl:text>local(</xsl:text>
	<xsl:value-of select="@Source"/>
	<xsl:text>) = </xsl:text>
	<xsl:text>wait_for (</xsl:text>
	<xsl:value-of select="@Source"/>
	<xsl:text>, </xsl:text>
	<xsl:value-of select="@Event"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="Read">
	<xsl:call-template name="Indent"/>
	<xsl:text>local(</xsl:text>
	<xsl:value-of select="@Source"/>
	<xsl:text>) = get(</xsl:text>
	<xsl:value-of select="@Source"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="While">
	<xsl:call-template name="Indent"/>
	<xsl:text>while </xsl:text>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="Switch">
	<xsl:call-template name="Indent"/>
	<xsl:text>switch (</xsl:text>
	<xsl:apply-templates select="child::*[position()=1]"/>
	<xsl:text>)</xsl:text>
	<xsl:call-template name="OpenBrace"/>
	<xsl:apply-templates select="child::*[position()>1]"/>
	<xsl:call-template name="CloseBrace"/>
</xsl:template>

<xsl:template match="Case">
	<xsl:call-template name="IndentLess"/>
	<xsl:text>case </xsl:text>
	<xsl:value-of select="@Value"/>
	<xsl:text>:</xsl:text>
	<xsl:apply-templates/>
	<xsl:call-template name="Indent"/>
	<xsl:text>break;&#10;</xsl:text>
</xsl:template>

<xsl:template match="Default">
	<xsl:call-template name="IndentLess"/>
	<xsl:text>default:</xsl:text>
	<xsl:apply-templates/>
	<xsl:call-template name="Indent"/>
	<xsl:text>break;&#10;</xsl:text>
</xsl:template>

<xsl:template match="Loop">
	<xsl:call-template name="Scope"/>
</xsl:template>

<xsl:template match="If">
	<xsl:call-template name="Indent"/>
	<xsl:text>if </xsl:text>
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="Then">
	<xsl:call-template name="Scope"/>
</xsl:template>

<xsl:template match="Else">
	<xsl:call-template name="Indent"/>
	<xsl:text>else</xsl:text>
	<xsl:call-template name="Scope"/>
</xsl:template>

<xsl:template match="Start">
	<xsl:call-template name="Indent"/>
	<xsl:text>start(</xsl:text>
	<xsl:value-of select="@Target"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="Stop">
	<xsl:call-template name="Indent"/>
	<xsl:text>stop(</xsl:text>
	<xsl:value-of select="@Target"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="End">
	<xsl:call-template name="Indent"/>
	<xsl:text>end();</xsl:text>
</xsl:template>

<xsl:template match="Call">
	<xsl:call-template name="Indent"/>
	<xsl:value-of select="@Target"/>
	<xsl:text>();</xsl:text>
</xsl:template>

<xsl:template match="Return">
	<xsl:call-template name="Indent"/>
	<xsl:text>return;</xsl:text>
</xsl:template>

<xsl:template match="Pause">
	<xsl:call-template name="Indent"/>
	<xsl:text>msleep(</xsl:text>
	<xsl:value-of select="@Value"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="SetBlockingFlag">
	<xsl:call-template name="Indent"/>
	<xsl:text>set_blocking(</xsl:text>
	<xsl:choose>
	<xsl:when test="@Mode='On'"><xsl:text>true</xsl:text></xsl:when>
	<xsl:when test="@Mode='Off'"><xsl:text>false</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:text>);</xsl:text>
</xsl:template>

<!--Counter-->

<xsl:template match="SetCounter">
	<xsl:call-template name="Indent"/>
	<xsl:text>set(</xsl:text>
	<xsl:value-of select="@Name"/>
	<xsl:text>,</xsl:text>
	<xsl:value-of select="@Value"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="IncCounter">
	<xsl:call-template name="Indent"/>
	<xsl:text>inc(</xsl:text>
	<xsl:value-of select="@Name"/>
	<xsl:text>,</xsl:text>
	<xsl:value-of select="@Value"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="CopyCounter">
	<xsl:call-template name="Indent"/>
	<xsl:text>set(</xsl:text>
	<xsl:value-of select="@Target"/>
	<xsl:text>,</xsl:text>
	<xsl:text>get(</xsl:text>	
	<xsl:value-of select="@Source"/>
	<xsl:text>)</xsl:text>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="AddCounter">
	<xsl:call-template name="Indent"/>
	<xsl:text>inc(</xsl:text>
	<xsl:value-of select="@Target"/>
	<xsl:text>,</xsl:text>
	<xsl:text>get(</xsl:text>	
	<xsl:value-of select="@Source"/>
	<xsl:text>)</xsl:text>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="SubCounter">
	<xsl:call-template name="Indent"/>
	<xsl:text>inc(</xsl:text>
	<xsl:value-of select="@Target"/>
	<xsl:text>,</xsl:text>
	<xsl:text>-get(</xsl:text>	
	<xsl:value-of select="@Source"/>
	<xsl:text>)</xsl:text>
	<xsl:text>);</xsl:text>
</xsl:template>

<!--Head-->

<xsl:template match="HeadTilt">
	<xsl:call-template name="Indent"/>
	<xsl:text>head_pan</xsl:text>
	<xsl:choose>
	<xsl:when test="@Type='Relative'"><xsl:text>_r(</xsl:text></xsl:when>
	<xsl:when test="@Type='Absolute'"><xsl:text>(</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:value-of select="@position"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="HeadTilt">
	<xsl:call-template name="Indent"/>
	<xsl:text>head_tilt</xsl:text>
	<xsl:choose>
	<xsl:when test="@Type='Relative'"><xsl:text>_r(</xsl:text></xsl:when>
	<xsl:when test="@Type='Absolute'"><xsl:text>(</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:value-of select="@Pos"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="HeadPan">
	<xsl:call-template name="Indent"/>
	<xsl:text>head_pan</xsl:text>
	<xsl:choose>
	<xsl:when test="@Type='Relative'"><xsl:text>_r(</xsl:text></xsl:when>
	<xsl:when test="@Type='Absolute'"><xsl:text>(</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:value-of select="@Pos"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="LedOn">
	<xsl:call-template name="Indent"/>
	<xsl:text>led_on(</xsl:text>
	<xsl:value-of select="@Mask"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="LedOff">
	<xsl:call-template name="Indent"/>
	<xsl:text>led_off(</xsl:text>
	<xsl:value-of select="@Mask"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="LedBlink">
	<xsl:call-template name="Indent"/>
	<xsl:text>led_blink(</xsl:text>
	<xsl:value-of select="@Mask"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<!--Arms-->

<xsl:template match="LeftArm">
	<xsl:call-template name="Indent"/>
	<xsl:text>left_arm</xsl:text>
	<xsl:choose>
	<xsl:when test="@Type='Relative'"><xsl:text>_r(</xsl:text></xsl:when>
	<xsl:when test="@Type='Absolute'"><xsl:text>(</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:value-of select="@Pos"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="RightArm">
	<xsl:call-template name="Indent"/>
	<xsl:text>right_arm</xsl:text>
	<xsl:choose>
	<xsl:when test="@Type='Relative'"><xsl:text>_r(</xsl:text></xsl:when>
	<xsl:when test="@Type='Absolute'"><xsl:text>(</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:value-of select="@Pos"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="LeftTool">
	<xsl:call-template name="Indent"/>
	<xsl:text>left_tool(</xsl:text>
	<xsl:choose>
	<xsl:when test="@Mode='On'"><xsl:text>true</xsl:text></xsl:when>
	<xsl:when test="@Mode='Off'"><xsl:text>false</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="RightTool">
	<xsl:call-template name="Indent"/>
	<xsl:text>right_tool(</xsl:text>
	<xsl:choose>
	<xsl:when test="@Mode='On'"><xsl:text>true</xsl:text></xsl:when>
	<xsl:when test="@Mode='Off'"><xsl:text>false</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:text>);</xsl:text>
</xsl:template>

<!--Base-->

<xsl:template match="Base">
	<xsl:call-template name="Indent"/>
	<xsl:choose>
	<xsl:when test="@Dir='Up'"><xsl:text>base_up()</xsl:text></xsl:when>
	<xsl:when test="@Dir='Down'"><xsl:text>base_down()</xsl:text></xsl:when>
	<xsl:when test="@Dir='Stop'"><xsl:text>base_stop()</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:text>;</xsl:text>
</xsl:template>

<xsl:template match="MoveSpeed">
	<xsl:call-template name="Indent"/>
	<xsl:text>move_speed(</xsl:text>
	<xsl:value-of select="@Speed"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="MoveSpeed">
	<xsl:call-template name="Indent"/>
	<xsl:text>move_speed(</xsl:text>
	<xsl:value-of select="@Speed"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="MoveDist">
	<xsl:call-template name="Indent"/>
	<xsl:text>move_dist(</xsl:text>
	<xsl:value-of select="@Dist"/>
	<xsl:text>,</xsl:text>
	<xsl:value-of select="@Speed"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="Rotate">
	<xsl:call-template name="Indent"/>
	<xsl:text>rotate(</xsl:text>
	<xsl:value-of select="@Speed"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="Turn">
	<xsl:call-template name="Indent"/>
	<xsl:text>turn(</xsl:text>
	<xsl:value-of select="@Angle"/>
	<xsl:text>,</xsl:text>
	<xsl:value-of select="@Speed"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="Lights">
	<xsl:call-template name="Indent"/>
	<xsl:text>lights(</xsl:text>
	<xsl:choose>
	<xsl:when test="@Mode='On'"><xsl:text>true</xsl:text></xsl:when>
	<xsl:when test="@Mode='Off'"><xsl:text>false</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:text>);</xsl:text>
</xsl:template>

<!--Voice-->

<xsl:template match="SayNumber">
	<xsl:call-template name="Indent"/>
	<xsl:text>say_number(</xsl:text>
	<xsl:value-of select="@Value"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="SayPhrase">
	<xsl:call-template name="Indent"/>
	<xsl:text>say_phrase(</xsl:text>
	<xsl:value-of select="@Index"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="PlaySound">
	<xsl:call-template name="Indent"/>
	<xsl:text>play_sound(</xsl:text>
	<xsl:value-of select="@Index"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="SayTemperature">
	<xsl:call-template name="Indent"/>
	<xsl:text>say_temperature(</xsl:text>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="MessagePlay">
	<xsl:call-template name="Indent"/>
	<xsl:text>message_play(</xsl:text>
	<xsl:value-of select="@Index"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="MessageDelete">
	<xsl:call-template name="Indent"/>
	<xsl:text>message_delete(</xsl:text>
	<xsl:value-of select="@Index"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="MessageRecord">
	<xsl:call-template name="Indent"/>
	<xsl:text>message_record(</xsl:text>
	<xsl:value-of select="@Index"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="MessageStop">
	<xsl:call-template name="Indent"/>
	<xsl:text>message_stop();</xsl:text>
</xsl:template>

<!--Outputs-->

<xsl:template match="LcdClear">
	<xsl:call-template name="Indent"/>
	<xsl:text>lcd_clear();</xsl:text>
</xsl:template>

<xsl:template match="LcdWriteText">
	<xsl:call-template name="Indent"/>
	<xsl:text>lcd_write_string(</xsl:text>
	<xsl:value-of select="@Row"/>
	<xsl:text>,</xsl:text>
	<xsl:value-of select="@Col"/>
	<xsl:text>,"</xsl:text>
	<xsl:value-of select="@Value"/>
	<xsl:text>");</xsl:text>
</xsl:template>

<xsl:template match="LcdWriteCounter">
	<xsl:call-template name="Indent"/>
	<xsl:text>lcd_write_int(</xsl:text>
	<xsl:value-of select="@Row"/>
	<xsl:text>,</xsl:text>
	<xsl:value-of select="@Col"/>
	<xsl:text>,</xsl:text>
	<xsl:text>get(</xsl:text>
	<xsl:value-of select="@Name"/>
	<xsl:text>)</xsl:text>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="SetOutput">
	<xsl:call-template name="Indent"/>
	<xsl:text>set_output(</xsl:text>
	<xsl:value-of select="@Index"/>
	<xsl:text>,</xsl:text>
	<xsl:choose>
	<xsl:when test="@Value='High'"><xsl:text>ACTIVE</xsl:text></xsl:when>
	<xsl:when test="@Value='Low'"><xsl:text>INACTIVE</xsl:text></xsl:when>
	<xsl:when test="@Value='SquareWave'"><xsl:text>SQUARE_WAVE</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:text>);</xsl:text>
</xsl:template>

<!-- Configurations -->

<xsl:template match="ConfigIO">
	<xsl:call-template name="Indent"/>
	<xsl:text>config_gpio(</xsl:text>
	<xsl:value-of select="@Index"/>
	<xsl:text>,</xsl:text>
	<xsl:choose>
	<xsl:when test="@Dir='Input'"><xsl:text>INPUT</xsl:text></xsl:when>
	<xsl:when test="@Dir='Output'"><xsl:text>OUTPUT</xsl:text></xsl:when>
	<xsl:when test="@Dir='Interrupt'"><xsl:text>INTERRUPT</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="ConfigFrequency">
	<xsl:call-template name="Indent"/>
	<xsl:text>config_gpio_freq(</xsl:text>
	<xsl:value-of select="@Value"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="SoundFollower">
	<xsl:call-template name="Indent"/>
	<xsl:text>sound_follower(</xsl:text>
	<xsl:choose>
	<xsl:when test="@Mode='On'"><xsl:text>true</xsl:text></xsl:when>
	<xsl:when test="@Mode='Off'"><xsl:text>false</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="TouchBehavior">
	<xsl:call-template name="Indent"/>
	<xsl:text>touch_behavior(</xsl:text>
	<xsl:choose>
	<xsl:when test="@Mode='On'"><xsl:text>true</xsl:text></xsl:when>
	<xsl:when test="@Mode='Off'"><xsl:text>false</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="ObstacleAvoidance">
	<xsl:call-template name="Indent"/>
	<xsl:text>obstacle_avoidance(</xsl:text>
	<xsl:choose>
	<xsl:when test="@Mode='On'"><xsl:text>true</xsl:text></xsl:when>
	<xsl:when test="@Mode='Off'"><xsl:text>false</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="VisionFollower">
	<xsl:call-template name="Indent"/>
	<xsl:text>vision_follower(</xsl:text>
	<xsl:choose>
	<xsl:when test="@Mode='On'"><xsl:text>true</xsl:text></xsl:when>
	<xsl:when test="@Mode='Off'"><xsl:text>false</xsl:text></xsl:when>
	</xsl:choose>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="SetTrackingColor">
	<xsl:call-template name="Indent"/>
	<xsl:text>set_tracking_color(</xsl:text>
	<xsl:value-of select="@Red"/>
	<xsl:text>,</xsl:text>
	<xsl:value-of select="@Green"/>
	<xsl:text>,</xsl:text>
	<xsl:value-of select="@Blue"/>
	<xsl:text>,</xsl:text>
	<xsl:value-of select="@Value"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="TakePicture">
	<xsl:call-template name="Indent"/>
	<xsl:text>take_picture();</xsl:text>
</xsl:template>

<xsl:template match="HandOpen">
	<xsl:call-template name="Indent"/>
	<xsl:text>hand_open();</xsl:text>
</xsl:template>

<xsl:template match="HandClose">
	<xsl:call-template name="Indent"/>
	<xsl:text>hand_close(</xsl:text>
	<xsl:value-of select="@Force"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="UrcSend">
	<xsl:call-template name="Indent"/>
	<xsl:text>urc_send(</xsl:text>
	<xsl:value-of select="@Index"/>
	<xsl:text>, </xsl:text>
	<xsl:value-of select="@Time"/>
	<xsl:text>);</xsl:text>
</xsl:template>

<xsl:template match="UrcStop">
	<xsl:call-template name="Indent"/>
	<xsl:text>urc_stop();</xsl:text>
</xsl:template>

</xsl:stylesheet>