import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import communication.transport.ConnectionProvider;

/**
 * Implementation of a connection provider on a client socket
 */
public class SocketProvider implements ConnectionProvider
{
	private Socket socket;

	private InputStream in;

	private OutputStream out;

	private int port;

	private String host;

	/**
	 * Create a client socket connection provider
	 * 
	 * @param host Remote server to connect to
	 * @param port Remote port to connect to
	 */
	public SocketProvider(String host, int port)
	{
		this.host = host;
		this.port = port;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see communication.transport.ConnectionProvider#getInputStream()
	 */
	public InputStream getInputStream()
	{
		return in;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see communication.transport.ConnectionProvider#getOutputStream()
	 */
	public OutputStream getOutputStream()
	{
		return out;
	}

	public int getStreamProperties()
	{
		// TODO controllare proprietÓ degli stream del socket
		return 0;
	}

	public boolean openConnection() throws IOException
	{
		socket = new Socket(host, port);
		if (socket == null)
			return false;
		in = socket.getInputStream();
		if (in == null)
			return false;
		out = socket.getOutputStream();
		if (out == null)
			return false;
		return true;
	}

	public void closeConnection() throws IOException
	{
		IOException ex = null;
		if (out != null)
		{
			try
			{
				out.close();
				out = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (in != null)
		{
			try
			{
				in.close();
				in = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (socket != null)
		{
			try
			{
				socket.close();
				socket = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (ex != null)
			throw ex;
	}
}
