import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import communication.transport.ConnectionProvider;

/**
 * Implementation of a connection provider for RxTx.org serial API
 */
public class SerialGnuIoProvider implements ConnectionProvider
{
	private SerialPort port;

	private String serialDevice;

	private int baudRate;

	private InputStream in;

	private OutputStream out;

	/**
	 * Create a serial connection provider
	 * 
	 * @param serialDevice
	 *            Device name (e.g. "COM1" on Win32 or "/dev/ttyS0" on Linux)
	 * @param baudRate
	 *            Communication speed (for real serial devices only)
	 */
	public SerialGnuIoProvider(String serialDevice, int baudRate)
	{
		this.serialDevice = serialDevice;
		this.baudRate = baudRate;
	}

	public InputStream getInputStream()
	{
		return in;
	}

	public OutputStream getOutputStream()
	{
		return out;
	}

	public int getStreamProperties()
	{
		return 0;// ConnectionProvider.AVAILABLE_IMPLEMENTED;
	}

	public boolean openConnection() throws IOException
	{
		CommPortIdentifier portId;
		try
		{
			portId = CommPortIdentifier.getPortIdentifier(serialDevice);
			if (portId == null)
				return false;
			port = (SerialPort) portId.open(getClass().getName(), 5000);
			if (port == null)
				return false;
			port.setSerialPortParams(baudRate, SerialPort.DATABITS_8,
				SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			port.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN
				| SerialPort.FLOWCONTROL_RTSCTS_OUT);
		}
		catch (Exception e)
		{
			throw new IOException(e.getMessage());
		}
		in = port.getInputStream();
		if (in == null)
			return false;
		out = port.getOutputStream();
		if (out == null)
			return false;
		return true;
	}

	public void closeConnection() throws IOException
	{
		IOException ex = null;
		if (out != null)
		{
			try
			{
				out.close();
				out = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (in != null)
		{
			try
			{
				in.close();
				in = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (port != null)
		{
			port.close();
			port = null;
		}
		if (ex != null)
			throw ex;
	}

	/**
	 * Enumerate available serial ports
	 * 
	 * @return A standard Enumeration interface object, to walk the list of
	 *         String objects which holds the names of serial ports
	 */
	public static Enumeration getPortList()
	{
		return new Enumeration()
		{
			Enumeration portList = CommPortIdentifier.getPortIdentifiers();
			CommPortIdentifier portId;

			public boolean hasMoreElements()
			{
				while (portList.hasMoreElements())
				{
					portId = (CommPortIdentifier) portList.nextElement();
					if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL)
						return true;
				}
				return false;
			}

			public Object nextElement()
			{
				return portId.getName();
			}
		};
	}
}
