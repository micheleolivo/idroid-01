import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

import communication.transport.ConnectionProvider;

/**
 * Implementation of a connection provider for J2ME Bluetooth transport
 */
public class BluetoothProvider implements ConnectionProvider
{
	private StreamConnection connection;

	private InputStream in;

	private OutputStream out;

	private String url;

	/**
	 * Create a Bluetooth connection provider
	 * 
	 * @param url
	 *            A connection URL to a remote SPP service (e.g.
	 *            "btspp:\\012345ABCDEF:1")
	 */
	public BluetoothProvider(String url)
	{
		this.url = url;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see communication.transport.ConnectionProvider#getStreamProperties()
	 */
	public int getStreamProperties()
	{
		// TODO controllare se available() funziona...
		return ConnectionProvider.READ_NON_INTERRUPTIBLE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see communication.transport.ConnectionProvider#getInputStream()
	 */
	public InputStream getInputStream()
	{
		return in;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see communication.transport.ConnectionProvider#getOutputStream()
	 */
	public OutputStream getOutputStream()
	{
		return out;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see communication.transport.ConnectionProvider#openConnection()
	 */
	public boolean openConnection() throws IOException
	{
		// opens the connection
		try
		{
			connection = (StreamConnection) Connector.open(url);
		}
		catch (SecurityException e)
		{
			return false;
		}
		if (connection == null)
			return false;
		in = connection.openInputStream();
		if (in == null)
			return false;
		out = connection.openOutputStream();
		if (out == null)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see communication.transport.ConnectionProvider#closeConnection()
	 */
	public void closeConnection() throws IOException
	{
		IOException ex = null;
		if (out != null)
		{
			try
			{
				out.close();
				out = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (in != null)
		{
			try
			{
				in.close();
				in = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (connection != null)
		{
			try
			{
				connection.close();
				connection = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (ex != null)
			throw ex;
	}
}
