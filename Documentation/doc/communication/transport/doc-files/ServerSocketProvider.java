import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import communication.transport.ConnectionProvider;

/**
 * Implementation of a connection provider on a server socket
 */
public class ServerSocketProvider implements ConnectionProvider
{
	private Socket socket;

	private InputStream in;

	private OutputStream out;

	private int port;

	private String host;

	private ServerSocket server;

	/**
	 * Create a server socket connection provider
	 * 
	 * @param host
	 *            Local address to listen to for incoming connections (null for
	 *            any local address)
	 * @param port
	 *            Local port to listen to
	 */
	public ServerSocketProvider(String host, int port)
	{
		this.host = host;
		this.port = port;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see communication.transport.ConnectionProvider#getInputStream()
	 */
	public InputStream getInputStream()
	{
		return in;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see communication.transport.ConnectionProvider#getOutputStream()
	 */
	public OutputStream getOutputStream()
	{
		return out;
	}

	public int getStreamProperties()
	{
		// TODO controllare proprietÓ degli stream del socket
		return 0;
	}

	public boolean openConnection() throws IOException
	{
		server = new ServerSocket(port, 1, (host == null || host.equals(""))
			? null : InetAddress.getByName(host));
		if (server == null)
			return false;
		socket = server.accept();
		server.close();
		server = null;
		if (socket == null)
			return false;
		in = socket.getInputStream();
		if (in == null)
			return false;
		out = socket.getOutputStream();
		if (out == null)
			return false;
		return true;
	}

	public void closeConnection() throws IOException
	{
		IOException ex = null;
		if (out != null)
		{
			try
			{
				out.close();
				out = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (in != null)
		{
			try
			{
				in.close();
				in = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (socket != null)
		{
			try
			{
				socket.close();
				socket = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (server != null)
		{
			try
			{
				server.close();
				server = null;
			}
			catch (IOException e)
			{
				ex = e;
			}
		}
		if (ex != null)
			throw ex;
	}
}
