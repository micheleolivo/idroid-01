/**
 * Questo programma pronuncia il numero del tasto premuto su un telecomando. 
 * Per poter funzionare occorre che il telecomando universale ad infrarossi sia installato
 * sul robot e che i segnali dei tasti da 0 (se disponibile) a 10 del telecomando siano stati 
 * appresi nelle corrispondenti posizioni.
 * Questo programma � una versione ottimizzata del programma urc_sample.vclike
 * 
 * This program pronunces the number of a key pressed on a remote control.
 * This program needs the Infrared Remote Control tool installed on the robot 
 * and that the infrared signal corresponding to the key from 0 (if available) to 10
 * of the remote control have been learned.  
 * This program is an optimised version of the program urc_sample.vclike
 * 
 * @author RoboTech srl
 * @version 1.0
 * 
 */

#include "c-like.h"
#include "robot.h"

#include <stdio.h>


counter Index = new(counter);

/* struttura per i codici ad infrarossi memorizzati sul robot */
/* structure for the ir codes stored on the robot */
urc_t ir_codes[10];

/* struttura per i codici toggle memorizzati sul robot */
/* structure for the ir toggle codes stored on the robot */
urc_t ir_toggles[10];

declare( behavior(WaitIR) );


define( behavior(Main))
{
	char file_name[80];
	int i=0;
	
	/* carica nelle strutture ir_codes e ir_toggles i codici normali
	 * e di toggle dei segnali appresi da 0 a 10 
	 */ 
	
	/* load the learned ir codes and the toggle codes from 0 to 10
	 * in the approproate strucutres
	 */
   
	while (i<10)
	{
		/* i segnali appresi sono contenuti nella cartella /mnt/disk 
		 * e il loro nome � nella forma 00X.dat, dove x � compreso tra 0 e 10.
		 */
		
		/* learned ir codes are stored in the folder /mnt/disk 
		 * and their names have the format 00X.dat, x between 0 and 10.
		 */
		
		sprintf(file_name,"/mnt/disk/ircodes/%03d.dat",i);
		urc_load(&ir_codes[i],file_name);

		/* i segnali di toggle sono contenuti nella cartella /mnt/disk 
		 * e il loro nome � nella forma 00X_t.dat, dove x � compreso tra 0 e 10.
		 */
		 
		/* learned ir toggle codes are stored in the folder /mnt/disk 
		 * and their names have the format 00X_t.dat, x between 0 and 10.
		 */

		sprintf(file_name,"/mnt/disk/ircodes/%03d_t.dat",i);
		urc_load(&ir_toggles[i],file_name);
		i++;
	}
	
	/* avvia il comportamento WaitIR che gestisce 
	 * la ricezione dei segnali ad infrarossi
	 */

	/* start the behavior WaitIR to manage
	 * the reception of ir codes
	 */

	start(WaitIR);
}

define( behavior(WaitIR) )
{
	int i=0;
	
	/* attende la ricezione di un segnale IR sul telecomando universale 
	 * ed effettua la copia di questo segnale nella variabile locale urc 
	 */
	 
	/* wait for the reception of a new ir code on the Universal Infrared Remote Control
	 * and copy it to the local strucutre urc
	 */
	local(urc) = wait_for (urc, update);
	
	/* confronta il segnale ricevuto contenuto in urc con i segnali memorizzati sul robot
	 * e precedentemente caricati nelle trutture ir_codes e ir_toggles
	 */

	/* compare the new received signal with the signal stored on the robot and 
	 * previously loaded in the ir_codes and ir_toggles structures
	 */ 
	while (i<10)
	{
		if ( ir_compare(&local(urc), &ir_codes[i]) || ir_compare(&local(urc), &ir_toggles[i]) )
		{
		/* se il segnale ricevuto coincide con il segnale normale o con il segnale di toggle
		 * pronuncia il numero corrispondente
		 */ 
		
		/* if the signal is equal pronunces the corresponding number */ 
			say_number(i);
			break;
		}
		i++;
	}
}