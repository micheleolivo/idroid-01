/**
 * Description
 * @author Name
 * @version 1.0
 */

#include "c-like.h"
#include "robot.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>

int pipeSend = -1;
int pipeRecv = -1;
pthread_mutex_t pipeMutex = PTHREAD_MUTEX_INITIALIZER;

/**
 * Setup communication channel with Java (blocking)
 */
void openJavaPipe(void)
{
	mkfifo("/var/Java_UP", 0666);
	mkfifo("/var/UP_Java", 0666);
	
	pipeRecv = open("/var/Java_UP", O_RDONLY);
	pipeSend = open("/var/UP_Java", O_WRONLY);
}

/**
 * Close communication channel with Java
 */
void closeJavaPipe(void)
{
	close(pipeRecv);
	close(pipeSend);
}

/**
 * Read a Java String from the pipe to the specified buffer
 * 
 * @param buffer A character buffer (long enough)
 * 
 * @return The number of bytes read, -1 for errors or EOF
 */
int readJavaString(char* buffer)
{
	int c1 = 0, c2 = 0, len, r, count;
	if (read(pipeRecv, &c1, 1) < 1) return -1;
	if (read(pipeRecv, &c2, 1) < 1) return -1;
	len = c2 | (c1 << 8);
	count = len;
	while (count > 0)
	{
		r = read(pipeRecv, buffer, count);
		if (r <= 0) return -1;
		count -= r;
		buffer += r;
	}
	*buffer = 0;
	return len;
}

/**
 * Write some text to the pipe as a Java String
 * 
 * @param buffer A string of text (null terminated)
 * 
 * @return Whether the operation was successful or not
 */
bool writeJavaString(const char* buffer)
{
	int c1, c2, len, w;
	len = strlen(buffer);
	if (len & 0xFFFF0000) return false;
	c1 = (len >> 8);
	c2 = (len & 255);
	pthread_mutex_lock(&pipeMutex);
	if (write(pipeSend, &c1, 1) < 1) return false;
	if (write(pipeSend, &c2, 1) < 1) return false;
	while (len > 0)
	{
		w = write(pipeSend, buffer, len);
		if (w <= 0) return false;
		len -= w;
	}
	pthread_mutex_unlock(&pipeMutex);
	return true;
}

// ---- Main Program ----

counter ConnStatus = new(counter);
define( event(ConnStatus), confirmation, _new_ >= 2 );

declare( behavior(Connector) );
declare( behavior(PipeReceiver) );
declare( behavior(VoiceCommander) );
declare( behavior(SoundCommander) );


define( behavior(Main))
{
	lcd_clear();
	start(Connector);
	lcd_write_string(1,1,"Waiting Java...");
	say_phrase(65);
	// wait for Java program to connect and
	// play "toy honk" effect every 5 seconds
	while (get(ConnStatus) < 1)
	{
		int i;
		for (i = 0; i < 50 && get(ConnStatus) < 1; ++i)
			msleep(100);
		if (get(ConnStatus) < 1)
			play_sound(9);
	}
	set(ConnStatus,2);
}

define( behavior(Connector) )
{
	openJavaPipe();

	set(ConnStatus,1);
	wait_for(ConnStatus, confirmation);

	lcd_write_string(1,1,"Java Connected!");
	say_phrase(66);
	writeJavaString("Hello, it's C-like!");

	start(PipeReceiver);
	start(VoiceCommander);
	start(SoundCommander);
	end();
}

define( behavior(PipeReceiver) )
{
	char text[100];
	while (readJavaString(text) >= 0)
	{
		lcd_write_string(2,1,"                ");
		lcd_write_string(2,1,text);
	}
	play_sound(1);
	stop(VoiceCommander);
	stop(SoundCommander);
	end();

	closeJavaPipe();
}

define( behavior(VoiceCommander) )
{
	local(voice_cmd) = wait_for (voice_cmd, update);
	switch (local(voice_cmd))
	{
	case CMD_ZERO:
		writeJavaString("Command Zero");
		break;

	case CMD_ONE:
		writeJavaString("Command One");
		break;

	case CMD_TWO:
		writeJavaString("Command Two");
		break;

	case CMD_THREE:
		writeJavaString("Command Three");
		break;

	case CMD_FOUR:
		writeJavaString("Command Four");
		break;

	case CMD_FIVE:
		writeJavaString("Command Five");
		break;

	case CMD_SIX:
		writeJavaString("Command Six");
		break;

	case CMD_SEVEN:
		writeJavaString("Command Seven");
		break;

	case CMD_EIGHT:
		writeJavaString("Command Eight");
		break;

	case CMD_NINE:
		writeJavaString("Command Nine");
		break;

	case CMD_TEN:
		writeJavaString("Command Ten");
		break;
	}
}

define( behavior(SoundCommander) )
{
	local(sound_dir) = wait_for (sound_dir, update);
	switch (local(sound_dir))
	{
	case SOUND_DIR_LEFT:
		writeJavaString("Sound Left");
		break;

	case SOUND_DIR_CLEFT:
		writeJavaString("Sound Mid-Left");
		break;

	case SOUND_DIR_CENTER:
		writeJavaString("Sound Front");
		break;

	case SOUND_DIR_CRIGHT:
		writeJavaString("Sound Mid-Right");
		break;

	case SOUND_DIR_RIGHT:
		writeJavaString("Sound Right");
		break;
	}
}